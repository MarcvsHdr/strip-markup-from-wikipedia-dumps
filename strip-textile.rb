#!/usr/bin/env ruby

# Author: Adrian Scoica (adrian.scoica@gmail.com)
#
# Please direct comments or questions to the e-mail address above.
# I am writing this script for my MPhil dissertation project, and I don't want
# to spend time with licensing headers. You can do whatever you want with it.
# However, if you do come up with a modification or improvement to the script,
# and would like to share it, please send me a patch and I will apply it to the
# repository, with proper credits and recognition (such as adding your name
# above).

require 'rubygems'
require 'wikicloth'
require 'nokogiri'
require 'timeout'

# Read in the input file, line by line.
$cleanedArticles = 0
$failedArticles = 0
$wikipediaWithTextile = ""
$wikipediaHTML = ""
$stdin.each_line do |l|
  if l == "##########_END_OF_ARTICLE_##########\n"
    begin
      # Remove <source> tags from the article.
      $wikipediaWithTextile.gsub!(/<source(.)+?<\/source>/m, "")

      # Cleanup the article and print it
      # (but don't allow more than 5 seconds to be spent on it)
      begin
        status = Timeout::timeout(10) do
          $wikipediaHTML = WikiCloth::Parser.new({ :data => $wikipediaWithTextile }).to_html
        end
      rescue Timeout::Error
        $stderr.puts "This is taking too much time. Aborting..."
        $failedArticles = failedArticles + 1
        $wikipediaHTML = ""
      else
        # Create an HTML document from the input, so that we can manipulate it later.
        doc = Nokogiri::HTML($wikipediaHTML)

        # We are stripping all scripts, images, tables and style tags.
        doc.search('script').each {|el| el.unlink}
        doc.search('img').each {|el| el.unlink}
        doc.search('table').each {|el| el.unlink}
        doc.search('style').each {|el| el.unlink}

        # Clean up the resulting document a bit.
        cleanArticle = doc.text
        # Remove inter-language links.
        cleanArticle.gsub!(/^([a-z]|-)+:(.*)$/, "")
        # Remove [edit] links before headings.
        cleanArticle.gsub!(/^\[edit\] /, "")
        # Remove [digit] reerences.
        cleanArticle.gsub!(/\[(\d)*\]/, "")
        # Remove "thumb" lines.
        cleanArticle.gsub!(/^thumb$/, "")
        # Remove consecutive empty lines.
        cleanArticle.gsub!(/((\r)?\n)+/m, "\n\n")

        # Print out the text version.
        puts cleanArticle
        $cleanedArticles = $cleanedArticles + 1
      end
    rescue
      $stderr.puts "Some other stranger error. Simply aborting..."
      $failedArticles = $failedArticles + 1
    ensure
      $stderr.puts "Cleaned articles: " + $cleanedArticles.to_s + " (+ " + $failedArticles.to_s + " failed)\n"
     # Cleans up the string for the next operaion.
      $wikipediaWithTextile = ""
    end
  else
    # Append this line to the previous line.
    $wikipediaWithTextile << l
  end
end
